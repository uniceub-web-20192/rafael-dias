package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type User struct {
	Email     string `json:"email"`
	Pass      string `json:"pass"`
	Birthdate string `json:"birthdate"`
}

type Database struct {
	Users []User
}

func (d *Database) AddUser(user User) {
	d.Users = append(d.Users, user)
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/register", RegisterHandler).
		Methods("POST").
		Headers("Content-Type", "application/json")

	r.MethodNotAllowedHandler = http.HandlerFunc(NotAllowedHandler)
	r.NotFoundHandler = http.HandlerFunc(NotFoundHandler)

	server := &http.Server{
		Addr:    "localhost:8080",
		Handler: r,
	}

	log.Fatal(server.ListenAndServe())
}
