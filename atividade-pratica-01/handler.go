package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func checkHeader(header http.Header, k, v string) bool {
	if dest, ok := header[k]; ok {
		for _, h := range dest {
			if h == v {
				return true
			}
		}
	}
	return false
}

func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	// Verificar header
	if !checkHeader(r.Header, "Content-Type", "application/json") {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid header"))
		return
	}
}

func NotAllowedHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
}

func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	var data User
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Deu ruim"))
		return
	}

	h := w.Header()
	h.Set("Content-Type", "application/json")
	fmt.Fprint(w, `{"message": "user successfully registered"}`)
}
