package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRegisterHandler(t *testing.T) {
	var correctJSON = []byte(`{
	"email": "teste@email.com",
	"pass": "123",
	"birthdate": "09/09/1995"
}`)

	// var incompleteJSON = []byte(`{
	// "email": "teste@email.com",
	// "pass": "123"
	// }`)

	t.Run("returns 200 and correct message on correct request", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest("POST", "/register", bytes.NewBuffer(correctJSON))
		req.Header.Add("Content-Type", "application/json")

		handler := http.HandlerFunc(RegisterHandler)
		handler.ServeHTTP(res, req)

		assertStatusCode(t, res.Code, http.StatusOK)

		got := res.Body.Bytes()
		want := []byte(`{"message": "user successfully registered"}`)
		assertBodyMessage(t, got, want)

		assertHeaderContent(t, res.Header(), "Content-Type", "application/json")
	})
}

func assertStatusCode(t *testing.T, got, want int) {
	t.Helper()

	if got != want {
		t.Errorf("Different status code.\nExpected: %d\nWant: %d\n", got, want)
	}
}

func assertBodyMessage(t *testing.T, got, want []byte) {
	t.Helper()

	if bytes.Compare(got, want) != 0 {
		t.Errorf("Wrong body message.\nGot: %s\nWant %s\n", got, want)
	}
}

func assertHeaderContent(t *testing.T, header http.Header, key, value string) {
	t.Helper()

	got := header.Get(key)

	if got != value {
		t.Errorf("Invalid header on response. Does not contain %s: %s", key, value)
	}

}
